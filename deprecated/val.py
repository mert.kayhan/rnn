#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 27 14:25:41 2017

@author: mertkayhan
"""

import random


def __static_vars__(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate


@__static_vars__(counter1=-1)
@__static_vars__(r=0.7)
def k_fold(data, label, fold=5):

    """
                        k-fold cross validation
    call this function k times to get the training and test splits
    """

    if(len(data) < fold):
        raise ValueError("The size of the data should be larger than \
                         or equal to the number of folds!")

    arr = [n for n in range(len(data))]
    random.shuffle(arr, lambda: k_fold.r)
    e_count = len(arr) // fold
    k_fold.counter1 += 1

    if(k_fold.counter1 == fold):
        raise ValueError("The counter cannot be equal to the \
                         number of folds!")

    test_split = arr[k_fold.counter1*e_count:k_fold.counter1*e_count+e_count]

    train_split = arr[0:k_fold.counter1*e_count]
    train_split += arr[k_fold.counter1*e_count+e_count:len(arr)]

    test_data = [data[i] for i in test_split]
    train_data = [data[i] for i in train_split]

    test_label = [label[i] for i in test_split]
    train_label = [label[i] for i in train_split]

    return train_data, test_data, train_label, test_label


@__static_vars__(counter2=-1)
def loocv(data, label):

    """
                    leave one out cross validation
    for x = sizeof data call this function x times to get the training
    and test splits
    """

    if(len(data) < 2):
        raise ValueError("The size of data cannot be less than 2!")

    loocv.counter2 += 1

    if(loocv.counter2 == len(data)):
        raise ValueError("The counter cannot be equal \
                         to the size of data!")

    test_data = data[loocv.counter2]
    test_label = label[loocv.counter2]
    train_data = [data[n] for n in range(len(data)) if n != loocv.counter2]
    train_label = [label[n] for n in range(len(data)) if n != loocv.counter2]

    return train_data, test_data, train_label, test_label

if(__name__ == "__main__"):

    print("s")
