#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  8 22:49:39 2017

@author: mertkayhan
"""

import pickle
import sys

debug = False

"""GET FILE NAME FROM TERMINAL"""

f_name = str(sys.argv[1])

data = []

"""OPEN TXT FILE AND SAVE IT IN A LIST AFTER REMOVING \n"""

with open(f_name, "r") as f:
    for line in f:
        if len(line) == 7:
            data.append(line.strip("\n")+".")

"""CREATE DESTINATION FILENAME"""

f_name = f_name.replace("word datasets/", "")
f_name = f_name.replace(".txt", ".pickle")

with open(f_name, 'wb') as handle:
    pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

"""READ THE SAVED DATA"""

if debug:
    with open(f_name, 'rb') as handle:
        b = pickle.load(handle)

    """SANITY CHECK"""

    print data == b
