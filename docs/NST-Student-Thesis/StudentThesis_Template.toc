\contentsline {chapter}{\numberline {1}Working with this Template}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Style and Expressions}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Subsection}{5}{subsection.1.1.1}
\contentsline {subsubsection}{Subsubsection}{5}{section*.2}
\contentsline {paragraph}{Paragraphs}{6}{section*.3}
\contentsline {section}{\numberline {1.2}Compiling}{6}{section.1.2}
\contentsline {section}{\numberline {1.3}Equations}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Figures}{6}{section.1.4}
\contentsline {section}{\numberline {1.5}Citations}{7}{section.1.5}
\contentsline {chapter}{\numberline {2}Introduction}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Problem Statement}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Related Work}{9}{section.2.2}
\contentsline {chapter}{\numberline {3}Main Part}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Design of your solution}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Implementation}{11}{section.3.2}
\contentsline {section}{\numberline {3.3}Experimental Results}{12}{section.3.3}
\contentsline {paragraph}{Statistics:}{12}{section*.4}
\contentsline {section}{\numberline {3.4}Discussion}{12}{section.3.4}
\contentsline {chapter}{\numberline {4}Conclusion}{13}{chapter.4}
\contentsline {chapter}{List of Figures}{15}{chapter.4}
\contentsline {chapter}{Bibliography}{17}{chapter*.5}
