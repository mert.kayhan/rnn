## Group Members

Mert Kayhan

Burakhan Koyuncu

Taylan Müslüman

## Task

Word completion using RNNs

## Method 

Comparison of bidirectional rnn and lstm using simple datasets

Training the superior approach on word data

## Using the software

python deploy.py < learned parameters >

The letter to be predicted should be replaced by "_". 

Currently we use words consisting of 6 letters, and therefore other input
lengths will be considered invalid.

To quit the program type "exit" or "EXIT". 