# -*- coding: utf-8 -*-
"""
Created on Sat Jul 08 15:23:17 2017

@author: burakhankoyuncu
"""
import numpy as np

import itertools as itert
#import matplotlib.pyplot as plt
#from pandas import read_csv
#import math
from keras.models import Model
from keras.layers import Input, LSTM, Dense, SimpleRNN, Reshape
#from sklearn.preprocessing import MinMaxScaler
#from sklearn.metrics import mean_squared_error
from sklearn.model_selection import KFold
from keras.utils import plot_model

KFOLD =False

data = np.load("training_datasets/vocab.enron.npy")
chars=data[()]['chars']
indices_char=data[()]['indices_char']
wordlen=data[()]['wordlen']
X=data[()]['X']
Y=data[()]['Y']
target_names=data[()]['target_names']

print "Number of sequences:",len(X)

X=X[:1200,:,:]
Y=Y[:1200,:,:]

print "Creating the Model..."
epochs=100
number_of_layers = 1
number_of_units = 120 #wordlen*len(chars)

inputs = Input(shape=(wordlen, len(chars)),name = 'input_layer')

if number_of_layers>1:
    x = LSTM(number_of_units,return_sequences=True,name = 'lstm_layer_1')(inputs) # wordlen * 2,3  // log10n
    
    for i in range(2,number_of_layers):
        name = 'lstm_layer_'+str(i)
        x = LSTM(number_of_units,return_sequences=True,name = name)(x) 
    
    name = 'lstm_layer_'+str(number_of_layers)  
    x = LSTM(number_of_units,name = name)(x) # wordlen * 2,3  // log10n
else:
    x = LSTM(number_of_units,name = 'lstm_layer_1')(inputs) # wordlen * 2,3  // log10n

    
dense = Dense(len(chars)*wordlen, activation='linear', name = 'output_layer')
reshape=Reshape((wordlen, len(chars)))

x = dense(x)
outputs = reshape(x)

model = Model(inputs=inputs, outputs=outputs)
model.compile(loss='mean_squared_error', optimizer='adam',\
                  metrics={'output_layer':'mean_absolute_error'})

print "Printing the Model..."
plot_model(model, to_file='LSTM_Model2.png',show_shapes=True, show_layer_names =True)

if KFOLD:
    k=5
    kf=KFold(n_splits=k, random_state=None, shuffle=True)
    
    print "Calculating KFolds..."
    err=[]
    for i,[train_index, test_index] in enumerate(kf.split(X)):
        
        print "Fitting the Fold ", i+1, "..."
        
        x_train, y_train = X[train_index,:,:], Y[train_index,:,:]
        x_test, y_test = X[test_index,:,:], Y[test_index,:,:]
    
        model.fit(x_train, y_train, epochs=epochs, batch_size=1, verbose=0)
        y_predict = model.predict(x_test)
        
        err_mean=np.mean(np.sum(np.sum((y_test-y_predict)**2,axis=1),axis=1), axis = 0)
        err.append(err_mean)
    
    err_folds = np.mean(err)
    print "MSE: ", err_folds

print "Fitting the whole Data..."

'''
def generate_arrays(X,Y):
    while 1:
        for i in range(X.shape[0]):
            x = X[i,:,:]
            y = Y[i,:,:]
            x = np.expand_dims(x,0)
            y = np.expand_dims(y,0)
            yield (x, y)
model.fit_generator(generate_arrays(X,Y), steps_per_epoch=1000, epochs=10, verbose=2)
'''

model.fit(X, Y, epochs=epochs, batch_size=1, verbose=2)


model.save("lstm_model")
Y_predict = model.predict(X)
err_all = np.mean(np.sum(np.sum((Y-Y_predict)**2,axis=1),axis=1), axis = 0)
print "MSE: ", err_all   
mw_idx=np.argmax(Y_predict,axis=2) #missing word idx
sz=mw_idx.shape

missing_words=[]
for i in range(sz[0]):
    word=[]
    for j in range(sz[1]):
        w=indices_char[mw_idx[i,j]]
        if w != ".":
            word.append(w)
        else:break
    missing_words.append("".join(word))

correct = 0.
for i,word in enumerate(missing_words):
    if word == target_names[i][:-1]:
        correct+=1
    else:
        print word,target_names[i][:-1]

accuracy = correct/float(len(missing_words))
print accuracy*100, "%"


#missing_words=["".join([indices_char[missing_words[i,j]] for j in range(sz[1])]) for i in range(sz[0])]
