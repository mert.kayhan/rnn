# -*- coding: utf-8 -*-
"""
Created on Tue Jul 04 15:27:02 2017

@author: burakhankoyuncu
"""
#TO DO: Implement forecast>0 , forecast = 0 means estimate xt from xt-n

import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
from keras.models import Model
from keras.layers import Input, LSTM, Dense, SimpleRNN
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.utils import plot_model


def prepare_data(data, scaler, training_ratio=0.67, lookback=1, forecast=0):
    
    training_size = int(len(data) * training_ratio)    
    training_data = data[0:training_size,:]
    test_data=data[training_size:,:]
    
    def time_data(data, lookback=1, forecast=0):
    
        x, y = [], []
        for i in range(len(data)-lookback-1):
            x.append(data[i:(i+lookback), 0])
            y.append(data[(i+lookback), 0])
        return np.array(x), np.array(y)
    
    training_x, training_y = \
            time_data(training_data, lookback=lookback, forecast=forecast)
    test_x, test_y = \
            time_data(test_data, lookback=lookback, forecast=forecast)
    
    training_x = np.reshape(training_x, (training_x.shape[0], 1, training_x.shape[1]))
    test_x = np.reshape(test_x, (test_x.shape[0], 1, test_x.shape[1]))

    return training_x, training_y, test_x, test_y
    
def plot_results(data, scaler, training_prediction, test_prediction, lookback=1, forecast=0):
    # shift training predictions for plotting
    plot1 = np.empty_like(data)
    plot1[:, :] = np.nan
    plot1[lookback:len(training_prediction)+lookback, :] = training_prediction
    # shift test predictions for plotting
    plot2 = np.empty_like(data)
    plot2[:, :] = np.nan
    plot2[len(training_prediction)+(lookback*2)+1:len(data)-1, :] = test_prediction
    # plot data and predictions
    plt.plot(data)
    plt.plot(plot1)
    plt.plot(plot2)
    plt.show()


    
def main(name,cols=0):
    np.random.seed(1234)
    def get_data(name,cols=0):
        if name =='lstm_example_dataset.csv':
            sep=';'
        else:
            sep=','
            
        return read_csv(name, usecols=[cols],sep=sep, engine='python', skipfooter=3)

    dataframe=get_data(name,cols)
    data = dataframe.values
    data = data.astype('float64')
    scaler = MinMaxScaler(feature_range=(0, 1))
    data = scaler.fit_transform(data)
    
    lookback=2
    training_x, training_y, test_x, test_y = prepare_data(data, scaler, lookback=lookback)
    
    print "Creating the Model..."
    inputs = Input(shape=(1, lookback),name = 'input_layer')
    lstm = LSTM(4)(inputs)
    outputs = Dense(1,activation='linear', name = 'output_layer')(lstm)
    model = Model(inputs=inputs, outputs=outputs)
    model.compile(loss='mean_squared_error', optimizer='adam')
    
    
    
    print "Printing the Model..."
    plot_model(model, to_file='LSTM_Model.png',show_shapes=True, show_layer_names =True)
    
    
    
    print "Fitting the Data..."
    model.fit(training_x, training_y, epochs=100, batch_size=1, verbose=2)
    
    
    
    print "Calculating Mean Squared Error..."
    training_prediction = scaler.inverse_transform(model.predict(training_x))
    training_target = scaler.inverse_transform([training_y])
    
    training_err =\
         math.sqrt(mean_squared_error(training_target[0,:], training_prediction[:,0]))
    
    test_prediction = scaler.inverse_transform(model.predict(test_x))
    test_target = scaler.inverse_transform([test_y])
    
    test_err =\
         math.sqrt(mean_squared_error(test_target[0,:], test_prediction[:,0]))
    
    print "Training: ", training_err, "Test: ", test_err 
    
    data=scaler.inverse_transform(data)
    print "Plotting Results..."
    plot_results(data, scaler, training_prediction, test_prediction, lookback, forecast=0)


if __name__ == "__main__":
    
    #name='lstm_example_dataset.csv'
    name='sea_ice.csv'

    cols=1
    main(name,cols)