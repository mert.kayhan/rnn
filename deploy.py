#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  9 00:42:24 2017

@author: mertkayhan
"""

import sys
from keras.models import load_model
import numpy as np

filepath = str(sys.argv[1])

model = load_model(filepath)

chars = ['.', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
         'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

word_len = 7


while True:
    invalid = False

    """get input"""
    word = raw_input("Please enter a word: ").lower() + "."

    """exit condition"""
    if word == "exit.":
        print "Exiting..."
        break

    """check input"""
    if "_" not in word:
        print 'The omitted letter should be replaced with "_"!'
        continue

    """check input length"""
    if len(word) != 7:
        print "Input lenght should be 6!"
        continue

    """network input"""
    net_in = np.zeros((1, word_len, len(chars)), dtype=np.bool)

    """check alphabet for validity && vectorize input"""
    for word_idx, char in enumerate(word):
        if char == "_":
            continue
        elif char not in chars:
            print "Input contains an unknown character!"
            print "Please try again..."
            invalid = True
            break
        else:
            letter_idx = char_indices[char]
            net_in[0, word_idx, letter_idx] = 1

    if invalid:
        continue

    """predict"""
    pred = model.predict(net_in)
    mw_idx = np.asarray(np.argmax(pred, axis=2))
    out = ""

    for e in mw_idx[0]:
        out += chars[e]

    print "Output: ", out[:-1]
