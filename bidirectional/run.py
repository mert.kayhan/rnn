#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  8 18:46:40 2017

@author: mertkayhan
"""

import keras_sequential
f_name = "sea_ice.npy"
# f_name = "simple_sequences.npy"
units = 100
fold = 5

if __name__ == "__main__":
    keras_sequential.main(f_name, units, fold)
