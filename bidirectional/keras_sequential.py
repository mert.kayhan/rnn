#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  1 18:25:39 2017

@author: mertkayhan
"""

from keras.layers.recurrent import SimpleRNN
from keras.layers.wrappers import Bidirectional
from keras.models import Sequential
from keras.utils import plot_model
from keras.layers import Dense, Activation
from keras.utils.vis_utils import model_to_dot
from IPython.display import SVG
import numpy as np
import sys
sys.path.append("../")
import val
import matplotlib.pyplot as plt

means = []
stds = []


def preprocess(x, j):
    it = x.shape[0]
    if j == 0:
        for i in xrange(it):
            stds.append(x[i, :].std(0))
            means.append(x[i, :].mean(0))
            x[i, :] = (x[i, :] - x[i, :].mean(0)) / x[i, :].std(0)
    elif j == 1:
        for i in xrange(it):
            stds.append(x[i, :, :].std(1))
            means.append(x[i, :, :].mean(1))
            x[i, :, :] = (x[i, :, :] - x[i, :, :].mean(1)) / x[i, :].std(1)
    else:
        return None
    return x


def split(x):
    num = x.shape[1] // 11
    data = np.zeros((2, num, 10))
    label = np.zeros((2, num))
    counter = 0
    for i in xrange(0, num*11, 11):
        data[:, counter, :] = x[:, i:i+10]
        label[:, counter] = x[:, i+10]
        counter += 1
    return data, label


def main(f_name, units, fold=5):
    data_ = np.load(f_name)
    if f_name == "sea_ice.npy":
        data_ = preprocess(data_, 0)
        data, target = split(data_)
        data = data.reshape((46, 10))
        target = target.reshape(46)
    if f_name == "simple_sequences.npy":
        data_ = preprocess(data_, 1)
        data = data_[:, :, :9]
        target = data_[:, :, 9]
    model = Sequential()
    if len(data.shape) == 2:
        model.add(Bidirectional(SimpleRNN(units, return_sequences=False),
                  batch_input_shape=(37, 1, 10)))
    else:
        model.add(Bidirectional(SimpleRNN(units, return_sequences=False),
                  batch_input_shape=(8, 1, 9)))
    model.add(Dense(1))
    model.add(Activation("linear"))
    # plot_model(model, to_file='model.png')
    # SVG(model_to_dot(model).create(prog='dot', format='svg'))
    model.compile(loss='mean_absolute_error', optimizer='adam')
    mae = np.zeros(fold)

    for k in xrange(fold):
        train_data, test_data, train_label, test_label = val.k_fold(data,
                                                                    target,
                                                                    fold)
        train_data = np.array(train_data)
        test_data = np.array(test_data)
        train_label = np.array(train_label)
        test_label = np.array(test_label)
        if len(train_data.shape) == 2:
            train_data = train_data.reshape((1,)+train_data.shape).transpose(1,
                                                                             0,
                                                                             2)
            test_data = test_data.reshape((1,)+test_data.shape).transpose(1, 0,
                                                                          2)

        model.fit(train_data, train_label, epochs=100, batch_size=1, verbose=0)

        pred = model.predict(test_data)
        if len(test_label.shape) == 2:
            mae[k] = np.sum(np.abs(test_label-pred)) / test_label.shape[0]
        else:
            mae[k] = np.sum(np.abs(test_label-pred.T)) / test_label.shape
        print "Error fold{}: {}".format(k+1, mae[k])

    print "Average error: ", np.sum(mae) / float(fold)
    model.save("model")

if __name__ == "__main__":
    print "s"
