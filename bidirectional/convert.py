#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  8 14:12:29 2017

@author: mertkayhan
"""

import numpy as np
from pandas import read_csv

#data = read_csv("sea_ice.csv", usecols=[1, 2],sep=",", engine='python', skipfooter=0)
#
#data = data.values
#data = data.astype('float64')
#data = np.array(data)
#print data
#np.save("sea_ice", data)

data = np.load("sea_ice.npy")

np.save("sea_ice.npy", data.T)