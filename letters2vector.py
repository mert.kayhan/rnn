# -*- coding: utf-8 -*-
"""
Created on Sun Jul 09 04:05:58 2017

@author: burak
"""

import numpy as np
import itertools as itert
import sys
import pickle
import numpy as np


#import prep

def prepare_data_letter2vec(voc="",chars=None,max_letter2estimate=1):
   
    if voc=="":return 0
    if chars ==None:
        chars = ['.', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    for _ in range(100):
        for i,word in enumerate(voc):
            for char in word:
                if char not in chars:
                    del voc[i]
                    break
    longestword= max(voc, key=len)
    wordlen=len(longestword)
    target_names =[]
    
    print "Characters:", chars
    print "Total Characters:", len(chars) 
    char_indices = dict((c, i) for i, c in enumerate(chars))
    indices_char = dict((i, c) for i, c in enumerate(chars))
    
    words=[]
    next_words=[]
    #next_chars=[]
    
    def combs(x,n):
        return [list(c) for i in range(1,n+1) for c in itert.combinations(x,i)]
    
    for word in voc:
        comb_idx = np.array(combs(range(len(word)), max_letter2estimate))
        #comb_idx = comb_idx[-2:]
        w=np.array(list(word))
        w_idx= np.array(range(len(word)))
        #print comb_idx
        #exit()
        for idx2del in comb_idx:
            flag = False
            for buff in idx2del:     
                if w[buff]==".":
                    flag = True
                    break
            if flag: continue

            target_names.append(word)
            
            wkeep = np.delete(w, idx2del)
            wkeep = "".join(wkeep.tolist())
            words.append([wkeep,np.delete(w_idx, idx2del)])
            
            next_words.append(word)
            #wdel=w[idx2del]
            #wdel = "".join(wdel.tolist())
            #next_chars.append([wdel,idx2del])
            #print wdel,wkeep,idx2del
    
    print "Number of Sequences:", len(words)
    def gen_letter2vec(chars, words, wordlen, char_indices):
        print "Vectorizing the Vocabulary..."
        X = np.zeros((len(words), wordlen, len(chars)), dtype=np.bool)
        Y = np.zeros((len(words), wordlen, len(chars)), dtype=np.bool)
        
        for i, [word, word_idx] in enumerate(words):
            
            letter_idx =[char_indices[char] for char in word]
            X[i, word_idx, letter_idx] = 1
                
            letter_idx =[char_indices[char] for char in next_words[i]]
            Y[i, range(len(letter_idx)), letter_idx] = 1 
            #letter_idx =[char_indices[char] for char in next_chars[i][0]]
            #Y[i, next_chars[i][1], letter_idx] = 1
        return X,Y
    
    X,Y= gen_letter2vec(chars, words, wordlen, char_indices)
    return chars,indices_char,wordlen, X, Y, target_names

def main():
    f_name = str(sys.argv[1])
    word_len=int(sys.argv[2])
    max_letter2estimate=int(sys.argv[3])
    f_name = "word_datasets/" + f_name
    voc = []
    voc_reduced = []
    """OPEN TXT FILE AND SAVE IT IN A LIST AFTER REMOVING \n"""

    with open(f_name, "r") as f:
        for line in f:
            if len(line) <= word_len and len(line) > 6:
                voc.append(line.strip("\n")+".")
    
    voc_reduced = voc[0::15]
    print "Number of words:", len(voc)
    print "Number of reduced words:", len(voc_reduced)

    chars, indices_char,wordlen, X, Y, target_names = \
        prepare_data_letter2vec(voc=voc_reduced,max_letter2estimate=max_letter2estimate)
    
    d={'chars': chars,'indices_char': indices_char,'wordlen':wordlen, 'X': X, 'Y':Y, 'target_names':target_names}
    f_name = f_name.replace("word_datasets/", "training_datasets/")
    
    with open(f_name, 'wb') as f:
        for word in voc_reduced:
            f.write(word+"\n")
    

    f_name = f_name.replace(".txt", ".npy")
    #print f_name
    np.save(f_name, d)
    
if __name__ == "__main__":
    main()    